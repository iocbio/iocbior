# iocbioR: utilities for use in Laboratory of Systems Biology

These are small utilities that are useful in the lab for data access
and analysis in R.

To install, make sure you have `devtools` package installed and then
run

```R
devtools::install_gitlab('iocbio/iocbior')
```

Currently implemented functions (see help in R):

- fetchSql - fetch data from the database
- setDbKey - for adding database password 

